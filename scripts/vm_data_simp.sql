/*******************************
 *   VUE MATERIALIZEE DES OBS  *
 *******************************/


/*
TODO : Ajouter la ligne suivant au cron "crontab -e" afin d'actualiser toutes les heures la vue
@hourly su postgres -c "psql -p 5434 dbchirowebgcra -c 'REFRESH MATERIALIZED VIEW views.vm_data_simp;'"
*/

set search_path to 'views';

drop materialized view if exists views.vm_data_simp 
;

create materialized view views.vm_data_simp as
	select
		ss.id_sighting
		, a.first_name
		, a.last_name
		, a.username
		, s2.name                                              as nom_localite
		, typeplace.code                                       as code_type_loc
		, typeplace.descr                                      as nom_type_loc
		, m2.code                                              as code_insee
		, m2.name                                              as nom_commune
		, s2.altitude                                          as altitude
		, s2.is_gite                                           as gite
		, ss.period                                            as periode
		, st_transform(s2.geom, 2154) :: geometry(point, 2154) as geom
		, st_x(st_transform(s2.geom, 2154)) :: int             as x
		, st_y(st_transform(s2.geom, 2154)) :: int             as y
		, c2.code
		, c2.descr
		, ses.name
		, ses.date_start
		, ds.codesp
		, ds.sp_true                                           as espece_vraie
		, ds.common_name_fr                                    as nom_vernaculaire
		, ds.sci_name                                          as nom_scientifique
		, ss.total_count                                       as nombre_total
		, ss.breed_colo                                        as colo_repro
		, concat('d', territory.code)                          as pguser_access
	from sights_sighting ss
		left join dicts_specie ds
			on ss.codesp_id = ds.id
		left join accounts_profile a
			on ss.observer_id = a.id
		left join sights_session ses
			on ss.session_id = ses.id_session
		left join sights_place s2
			on ses.place_id = s2.id_place
		left join dicts_typeplace typeplace
			on s2.type_id = typeplace.id
		left join dicts_contact c2
			on ses.contact_id = c2.id
		left join geodata_territory territory
			on st_within(s2.geom, territory.geom)
		left join geodata_municipality m2
			on s2.municipality_id = m2.id
	where ss.is_doubtful is false
;

create index on views.vm_data_simp (id_sighting)
;

create index on views.vm_data_simp
using GIST (geom)
;

create index on views.vm_data_simp (pguser_access)
;

create index on views.vm_data_simp (codesp)
;

create index on views.vm_data_simp (nom_commune)
;

create index on views.vm_data_simp (code_insee)
;

create index on views.vm_data_simp (nom_localite)
;

create index on views.vm_data_simp (code_type_loc)
;

create index on views.vm_data_simp (espece_vraie)
;

create index on views.vm_data_simp (nom_scientifique)
;

create index on views.vm_data_simp (nom_vernaculaire)
;

select populate_geometry_columns()
;

refresh materialized view views.vm_data_simp
;

/*******************************
 *   VUE  DES OBS              *
 *******************************/
drop view views.data_simp
;

create view views.data_simp as
	select
		ss.id_sighting
		, a.first_name
		, a.last_name
		, a.username
		, s2.name                                              as nom_localite
		, typeplace.code                                       as code_type_loc
		, typeplace.descr                                      as nom_type_loc
		, m2.code                                              as code_insee
		, m2.name                                              as nom_commune
		, st_transform(s2.geom, 2154) :: geometry(point, 2154) as geom
		, st_x(st_transform(s2.geom, 2154)) :: int             as x
		, st_y(st_transform(s2.geom, 2154)) :: int             as y
		, c2.code
		, c2.descr
		, ses.name
		, ses.date_start
		, ds.codesp
		, ds.sp_true                                           as espece_vraie
		, ds.common_name_fr                                    as nom_vernaculaire
		, ds.sci_name                                          as nom_scientifique
		, ss.total_count                                       as nombre_total
		, ss.breed_colo                                        as colo_repro
		, ss.is_doubtful                                       as douteux
	from sights_sighting ss
		left join dicts_specie ds
			on ss.codesp_id = ds.id
		left join accounts_profile a
			on ss.observer_id = a.id
		left join sights_session ses
			on ss.session_id = ses.id_session
		left join sights_place s2
			on ses.place_id = s2.id_place
		left join dicts_typeplace typeplace
			on s2.type_id = typeplace.id
		left join dicts_contact c2
			on ses.contact_id = c2.id
		left join geodata_territory territory
			on st_within(s2.geom, territory.geom)
		left join geodata_municipality m2
			on s2.municipality_id = m2.id
;



