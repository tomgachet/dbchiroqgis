-- synthèse communale données
/* buts :
  - couche commune : nom commune, nb data, nb sp, nb loc
  - couche synthèse : periode contact + nb data, type contact + nombre de données, nombre de gîtes, colo_repro, nb de localités
 */
/*creation de la vue des communes avec données*/
set search_path to 'views';

DROP MATERIALIZED VIEW views.synth_comm_list;
CREATE MATERIALIZED VIEW views.synth_comm_list AS (
  SELECT
    m.id,
    m.name,
    count(DISTINCT ss.id_sighting)                                                    nb_data,
    count(DISTINCT ds.id)                                                             nb_sp,
    count(DISTINCT ses.place_id)                                                      nb_loc,
    min(extract(YEAR FROM date_start)) || ' - ' || max(extract(YEAR FROM date_start)) amplitude,
    m.geom
  FROM geodata_municipality m
    INNER JOIN public.sights_place sp ON sp.municipality_id = m.id
    INNER JOIN public.sights_session ses ON ses.place_id = sp.id_place
    LEFT JOIN sights_sighting ss ON ses.id_session = ss.session_id
    LEFT JOIN dicts_specie ds ON ss.codesp_id = ds.id
    LEFT JOIN PUBLIC.geodata_territory t ON t.id = sp.territory_id
      WHERE t.coverage IS TRUE and (ds.sp_true is true or ds.codesp ilike 'murmur')
  GROUP BY m.name, m.geom, m.id
);

/*couche de synthèse*/
DROP MATERIALIZED VIEW views.synth_comm_detail;
CREATE MATERIALIZED VIEW views.synth_comm_detail AS (WITH
    list_comm AS (
    /*list des communes avec données et filtrage sur sp vraies*/
      SELECT
        m.id,
        ds.codesp,
        CASE WHEN bool_or(ss.breed_colo) IS TRUE
          THEN 'oui'
        ELSE 'non' END                                                  repro,
        count(DISTINCT p.id_place)                                      nb_loc,
        string_agg(DISTINCT (a.last_name || ' ' || a.first_name), ', ') observateurs
      FROM public.geodata_municipality m
        INNER JOIN public.sights_place p ON m.id = p.municipality_id
        INNER JOIN PUBLIC.sights_session ses ON ses.place_id = p.id_place
        LEFT JOIN PUBLIC.sights_sighting ss ON ses.id_session = ss.session_id
        LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
        LEFT JOIN public.accounts_profile a ON ses.main_observer_id = a.id
        LEFT JOIN PUBLIC.sights_session_other_observer ssoo ON ssoo.session_id = ses.id_session
        LEFT JOIN public.accounts_profile a2 ON ssoo.profile_id = a2.id
      WHERE ds.sp_true IS TRUE OR codesp ILIKE 'murmur'
      GROUP BY m.id, ds.codesp
  ),
    sp_comm_gite AS (
      SELECT DISTINCT
        m.id,
        ds.codesp,
        count(DISTINCT p.id_place) nb_gite
      FROM public.geodata_municipality m
        INNER JOIN public.sights_place p ON m.id = p.municipality_id
        INNER JOIN PUBLIC.sights_session ses ON ses.place_id = p.id_place
        LEFT JOIN PUBLIC.sights_sighting ss ON ses.id_session = ss.session_id
        LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
      WHERE (ds.sp_true IS TRUE OR codesp ILIKE 'murmur') AND is_gite IS TRUE
      GROUP BY m.id, ds.codesp
  ),
    sp_gite_an as (
    SELECT
        m2.id,
        ds.codesp,
        min(extract(YEAR FROM date_start)) an_mini,
        max(extract(YEAR FROM date_start)) an_max
      FROM PUBLIC.sights_sighting ss
        LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
        LEFT JOIN PUBLIC.sights_session ses ON ss.session_id = ses.id_session
        LEFT JOIN PUBLIC.sights_place s2 ON ses.place_id = s2.id_place
        LEFT JOIN PUBLIC.geodata_municipality m2 ON s2.municipality_id = m2.id
      GROUP BY m2.id, ds.codesp
  ),
    sp_period AS (
    /*genere une liste de commune, espèce, nombre de données par période*/
      SELECT
        m2.id,
        ds.codesp,
        CASE
        WHEN ss.period ILIKE 'Trans%'
          THEN 'T'
        WHEN ss.period ILIKE 'Est%'
          THEN 'E'
        WHEN ss.period ILIKE 'Hiver%'
          THEN 'H'
        END                                period_transf,
        COUNT(DISTINCT ss.id_sighting)     nb_data
      FROM PUBLIC.sights_sighting ss
        LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
        LEFT JOIN PUBLIC.sights_session ses ON ss.session_id = ses.id_session
        LEFT JOIN PUBLIC.sights_place s2 ON ses.place_id = s2.id_place
        LEFT JOIN PUBLIC.geodata_municipality m2 ON s2.municipality_id = m2.id
      GROUP BY m2.id, ds.codesp, period_transf
  ),
    sp_period_agreg AS (
      SELECT
        id,
        codesp,
        string_agg(period_transf || '(' || nb_data || ')', ', ') nb_data_period
      FROM sp_period
      GROUP BY id, codesp
  ),
    sp_contact AS (
    /* génère une liste de communes, espèces, type de contact et nombre de données*/
      SELECT
        m2.id,
        ds.codesp,
        c2.code,
        count(DISTINCT (ss.id_sighting)) nb_data
      FROM PUBLIC.sights_sighting ss
        LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
        LEFT JOIN PUBLIC.sights_session ses ON ss.session_id = ses.id_session
        LEFT JOIN PUBLIC.sights_place s2 ON ses.place_id = s2.id_place
        LEFT JOIN PUBLIC.dicts_typeplace tp ON s2.type_id = tp.id
        LEFT JOIN PUBLIC.dicts_contact c2 ON ses.contact_id = c2.id
        LEFT JOIN PUBLIC.geodata_municipality m2 ON s2.municipality_id = m2.id
      GROUP BY m2.id, ds.codesp, c2.code
  ),
    sp_contact_agreg AS (
      SELECT
        id,
        codesp,
        string_agg(code || '(' || nb_data || ')', ', ') nb_data_contac
      FROM sp_contact
      GROUP BY id, codesp
  )
SELECT distinct
  row_number()
  OVER (
    ORDER BY lm.id )                              row_number,
  lm.id,
  dsp.codesp,
  string_agg(DISTINCT (spp.nb_data_period), ', ') nb_data_period,
  string_agg(DISTINCT (spc.nb_data_contac), ', ') nb_data_contac,
  coalesce(spg.nb_gite, 0)                        nb_gite,
  lm.repro,
  lm.nb_loc,
  spga.an_mini||'-'||spga.an_max                             periode,
  lm.observateurs
FROM
  list_comm lm
  LEFT JOIN sp_period_agreg spp ON lm.id = spp.id AND lm.codesp = spp.codesp
  left join sp_period sp on lm.id = sp.id AND lm.codesp = sp.codesp
  LEFT JOIN sp_contact_agreg spc ON spc.id = lm.id AND lm.codesp = spc.codesp
  LEFT JOIN dicts_specie dsp ON dsp.codesp = spp.codesp OR dsp.codesp = spc.codesp
  LEFT JOIN sp_comm_gite spg ON lm.id = spg.id AND lm.codesp = spg.codesp
  left join sp_gite_an spga on spga.id=lm.id and spga.codesp=lm.codesp
WHERE spp.nb_data_period IS NOT NULL OR spc.nb_data_contac IS NOT NULL
GROUP BY lm.id, dsp.codesp, lm.repro, lm.nb_loc, lm.observateurs, spg.nb_gite, periode
);
/*les vues sont stockées dans un schéma views et l'utilisateur pouvant les consulter actuellement est consultall*/
GRANT USAGE ON SCHEMA public, views TO consultall;
GRANT SELECT ON ALL TABLES IN SCHEMA public, views TO consultall;

