/*creation d'un table de gîte avec leur type et qq infos annexes*/
set search_path to 'views';

DROP MATERIALIZED VIEW views.synth_gite_list;
CREATE MATERIALIZED VIEW views.synth_gite_list AS (
  SELECT
    p.id_place,
    com.name                                                                          commune,
       CASE
                        WHEN tp.category ILIKE 'tree'
                                THEN 'Arbre'
                        WHEN tp.category ILIKE 'building'
                                THEN 'Bâtiment'
                        WHEN tp.category ILIKE 'cliff'
                                THEN 'Falaise'
                        WHEN tp.category ILIKE 'nest'
                                THEN 'Gîte artificiel'
                        WHEN tp.category ILIKE 'bridge'
                                THEN 'Pont'
                        WHEN tp.category ILIKE 'cave'
                                THEN 'Grotte'
                        WHEN tp.category ILIKE 'other'
                                THEN 'Autre'
                        ELSE NULL END   type_gite,
    p.name                                                                            nom_loc,
    count(DISTINCT ds.id)                                                             nb_sp,
    count(DISTINCT ss.id_sighting)                                                    nb_data,
    min(extract(YEAR FROM date_start)) || '-' || max(extract(YEAR FROM date_start)) amplitude,
    p.geom
  FROM sights_place p
    LEFT JOIN geodata_municipality com ON com.id = p.municipality_id
    LEFT JOIN dicts_typeplace tp ON p.type_id = tp.id
    LEFT JOIN sights_session s ON p.id_place = s.place_id
    LEFT JOIN sights_sighting ss ON ss.session_id = s.id_session
    LEFT JOIN dicts_specie ds ON ds.id = ss.codesp_id
    LEFT JOIN geodata_territory t on p.territory_id = t.id
  WHERE p.is_gite = 'true' AND (s.contact_id in (1,5,6,7,9)) and t.coverage is true
  GROUP BY p.id_place, com.name, tp.category, p.name, p.geom
);
/*objectif : avoir pour les gîtes l'effectif max par période et par espèce et le nombre de données*/
--DROP MATERIALIZED VIEW views.gite_eff_max_periode;
drop MATERIALIZED VIEW  views.synth_gite_detail;
CREATE MATERIALIZED VIEW views.synth_gite_detail AS (
  WITH
      data_gite AS (
        SELECT
          ss.*,
          p.id_place
        FROM sights_sighting ss
          LEFT JOIN sights_session ses ON ss.session_id = ses.id_session
          LEFT JOIN sights_place p ON ses.place_id = p.id_place
          LEFT JOIN dicts_contact c2 ON ses.contact_id = c2.id
        WHERE p.is_gite = 'true' AND c2.code not in ('vm','du')
    )/*,
      listloc AS (*/
        SELECT
          p.id_place,
          dg.codesp_id,
          min(extract(YEAR FROM date_start)) || '-' || max(extract(YEAR FROM date_start)) amplitude,
          string_agg(DISTINCT (a.last_name || ' ' || a.first_name), ', ')                   observateurs,
          p.geom,
          max(dg.id_sighting)                                                               id_ss
        FROM sights_place p
          LEFT JOIN geodata_municipality com ON com.id = p.municipality_id
          LEFT JOIN dicts_typeplace tp ON p.type_id = tp.id
          LEFT JOIN sights_session ses ON p.id_place = ses.place_id
          LEFT JOIN data_gite dg ON dg.session_id = ses.id_session
          LEFT JOIN geodata_territory t ON st_within(p.geom, t.geom)
          LEFT JOIN public.accounts_profile a ON ses.main_observer_id = a.id
          LEFT JOIN PUBLIC.sights_session_other_observer ssoo ON ssoo.session_id = ses.id_session
          LEFT JOIN public.accounts_profile a2 ON ssoo.profile_id = a2.id
          LEFT JOIN dicts_contact c3 ON ses.contact_id = c3.id
        WHERE p.is_gite = 'true' AND c3.code not in ('vm','du') and t.coverage is true/*and t.code='01'*/
        GROUP BY p.id_place, com.name, p.name, p.geom, dg.codesp_id
    ),
      datatransit AS (
        SELECT
          codesp_id,
          id_place,
          count(DISTINCT id_sighting) nb_data,
          max(total_count)
        FROM data_gite
        WHERE period ILIKE 't%'
        GROUP BY codesp_id, id_place
    ),
      datahiver AS (
        SELECT
          codesp_id,
          id_place,
          count(DISTINCT id_sighting) nb_data,
          max(total_count)
        FROM data_gite
        WHERE period ILIKE 'hiver%'
        GROUP BY codesp_id, id_place
    ),
      dataete AS (
        SELECT
          codesp_id,
          id_place,
          count(DISTINCT id_sighting) nb_data,
          max(total_count),
          CASE WHEN bool_or(breed_colo) IS TRUE
            THEN 'oui'
          ELSE 'non' END              repro
        FROM data_gite
        WHERE period ILIKE 'est%'
        GROUP BY codesp_id, id_place
    )
  SELECT
    ll.id_ss,
    ds.codesp,
    ll.id_place,
    de.max || ' ind max (n=' || de.nb_data || ')' ete,
    dh.max || ' ind max (n=' || dh.nb_data || ')' hiver,
    dt.max || ' ind max (n=' || dt.nb_data || ')' transit,
    de.repro,
    ll.amplitude,
    ll.observateurs
  FROM listloc ll
    LEFT JOIN datahiver dh ON ll.id_place = dh.id_place AND ll.codesp_id = dh.codesp_id
    LEFT JOIN dataete de ON ll.id_place = de.id_place AND ll.codesp_id = de.codesp_id
    LEFT JOIN datatransit dt ON ll.id_place = dt.id_place AND ll.codesp_id = dt.codesp_id
    LEFT JOIN dicts_specie ds ON ll.codesp_id = ds.id
);

GRANT USAGE ON SCHEMA public, views TO consultall;
GRANT SELECT ON ALL TABLES IN SCHEMA public, views TO consultall;

select * from information_schema.columns where table_name ilike 'synth%';

