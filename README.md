# [WIP] Projet QGIS d'utililisation des données dbChiro.

## Génération de tables et vues de synthèse
Adapter et éxécuter les scripts SQL du dossier `scripts`

## Création du projet QGIS

* Copier le fichier `config.ini.sample` en `config.ini`
* Editez les paramètres du fichier `config.ini`
* Exécutez le scripts `setup_qgis_project.sh`

